package com.example.agendalite;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import database.AgendaContacto;
import database.AgendaDBHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {

    private AgendaContacto db;
    private Contacto savedContact;
    private int id;

    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNota;
    private CheckBox chkFavorito;

    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnListar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db= new AgendaContacto(MainActivity.this);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtTel1 = (EditText)findViewById(R.id.txtTel1);
        txtTel2 = (EditText)findViewById(R.id.txtTel2);
        txtDomicilio = (EditText)findViewById(R.id.txtDomicilio);
        txtNota = (EditText)findViewById(R.id.txtNota);
        chkFavorito = (CheckBox)findViewById(R.id.chkFavorito);

        btnGuardar = (Button)findViewById(R.id.btnGuardar);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);
        btnListar = (Button)findViewById(R.id.btnListar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarDatos()){
                    guardarContacto();
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCajas();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                startActivityForResult(intent,0);
            }
        });
    }

    public void limpiarCajas(){
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtDomicilio.setText("");
        txtNota.setText("");
        chkFavorito.setChecked(false);
    }

    public boolean validarDatos(){
        boolean b = true;
        if(txtNombre.getText().toString().trim().isEmpty()){
            Toast.makeText(MainActivity.this,"Por favor ingrese un nombre",Toast.LENGTH_SHORT).show();
            txtNombre.setText("");
            txtNombre.requestFocus();
            b = false;
        }
        else if(txtTel1.getText().toString().trim().isEmpty()){
            Toast.makeText(MainActivity.this,"Por favor ingrese un numero de telefono",Toast.LENGTH_SHORT).show();
            txtTel1.setText("");
            txtTel1.requestFocus();
            b = false;
        }
        else if(txtDomicilio.getText().toString().trim().isEmpty()){
            Toast.makeText(MainActivity.this,"Por favor ingrese su direccion",Toast.LENGTH_SHORT).show();
            txtTel2.setText("");
            txtDomicilio.requestFocus();
            b = false;
        }
        return b;
    }

    public void guardarContacto(){
        int favorito;
        String nombre = txtNombre.getText().toString();
        String tel1 = txtTel1.getText().toString();
        String tel2 = txtTel2.getText().toString();
        String domicilio = txtDomicilio.getText().toString();
        String nota = txtNota.getText().toString();
        if(chkFavorito.isChecked()){
            favorito = 1;
        }
        else {
            favorito = 2;
        }
        db.opedDatabase();
        Contacto c = new Contacto(nombre,tel1,tel2,domicilio,nota,favorito);
        if(savedContact == null){
            long idx = db.insertarContacto(c);
            Toast.makeText(MainActivity.this,"Se agrego el contacto con ID: "+idx, Toast.LENGTH_SHORT).show();
        }
        else{
            db.actualizarContacto(c,id);
            Toast.makeText(MainActivity.this,"Se actualizo el registro: "+id, Toast.LENGTH_SHORT).show();
        }
        db.cerrar();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(Activity.RESULT_OK == resultCode){
            Contacto contacto = (Contacto)data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = (int)contacto.getID();
            txtNombre.setText(contacto.getNombre());
            txtTel1.setText(contacto.getTelefono1());
            txtTel2.setText(contacto.getTelefono2());
            txtDomicilio.setText(contacto.getDomicilio());
            txtNota.setText(contacto.getNotas());
            if(contacto.getFavorito() > 0){
                chkFavorito.setChecked(true);
            }
        }else{
            limpiarCajas();
        }
    }
}
