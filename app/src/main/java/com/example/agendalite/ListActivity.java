package com.example.agendalite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import database.AgendaContacto;
import database.Contacto;

public class ListActivity extends android.app.ListActivity{

    private AgendaContacto agendaContacto;
    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;
    private Button btnNuevo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        btnNuevo= (Button)findViewById(R.id.btnNuevo);
        agendaContacto = new AgendaContacto(this);
        llenarLista();
        adapter = new MyArrayAdapter(this,R.layout.layout_contacto,listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void llenarLista(){
        agendaContacto.opedDatabase();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrar();
    }

    //Clase anidada dentro del ListActivity
    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int texViewResourcedId;
        private ArrayList<Contacto> contactos;

        public MyArrayAdapter(@NonNull Context context, int resource, ArrayList<Contacto>  contactos) {
            super(context, resource);
            this.context = context;
            this.texViewResourcedId = resource;
            this.contactos = contactos;
        }

        public View getView(final int position, View converView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.texViewResourcedId,null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombre);
            TextView lblTelefono = (TextView)findViewById(R.id.lblTel1);

            Button btnModificar = (Button)findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito() > 0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContacto.opedDatabase();
                    agendaContacto.eliminarContacto(contactos.get(position).getID());
                    agendaContacto.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto",contactos.get(position));
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
            });
            return  view;
        }
    }
}
