package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AgendaDBHelper extends SQLiteOpenHelper {

    private static final String TEXT_TYPE = " TEXT ";
    private static final String INTEGER_TYPE =" INTEGER ";
    private static final String COMMA=" ,";
    private static final String SQL_CREATE_CONTACTO=" CREATE TABLE " +
            DefinirTabla.Contacto.TABLE_NAME + " ("+
            DefinirTabla.Contacto._ID + " INTEGER PRIMARY KEY, " +
            DefinirTabla.Contacto.NOMBRE + TEXT_TYPE + COMMA +
            DefinirTabla.Contacto.DOMICILIO + TEXT_TYPE + COMMA +
            DefinirTabla.Contacto.TELEFONO1 + TEXT_TYPE + COMMA +
            DefinirTabla.Contacto.TELEFONO2 + TEXT_TYPE + COMMA +
            DefinirTabla.Contacto.NOTAS + TEXT_TYPE + COMMA +
            DefinirTabla.Contacto.FAVORITO + INTEGER_TYPE + ")";
    private static final String SQL_DELETE_CONTACTO = " DROP TABLE IF EXISTS " + DefinirTabla.Contacto.TABLE_NAME;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "agenda.db";

    public AgendaDBHelper(@Nullable Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);
    }
}
